# android-CheckBox-practice

【2016-08-04】 

練習Android Checkbox使用。

預覽圖如下：

![](https://gitlab.com/w860403/android-checkbox-practice/raw/master/doc/android-CheckBox-practice.PNG)

【教學】

1. 建立`TextView`、`CheckBox`、`Button`。
2. 取得各個物件ID：`findViewById()`。
3. 建立按鈕監聽事件：`setOnClickListener()`。