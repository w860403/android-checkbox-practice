package com.example.user.test02;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CheckBox movie,reading,running,painting,writing;
    private Button commitBtn;
    private TextView msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        movie=(CheckBox)findViewById(R.id.checkBoxMovie);
        reading=(CheckBox)findViewById(R.id.checkBoxReading);
        running=(CheckBox)findViewById(R.id.checkBoxRunning);
        painting=(CheckBox)findViewById(R.id.checkBoxPainting);
        writing=(CheckBox)findViewById(R.id.checkBoxWriting);
        commitBtn=(Button)findViewById(R.id.commitBtn);
        msg=(TextView)findViewById(R.id.msg);

        commitBtn.setOnClickListener(commintBtnListener);

    }

    private View.OnClickListener commintBtnListener=new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            String str="";
            if(movie.isChecked())
                str +=getString(R.string.movie);
            if(reading.isChecked())
                str +=getString(R.string.reading);
            if(running.isChecked())
                str +=getString(R.string.running);
            if(writing.isChecked())
                str +=getString(R.string.writing);
            if(painting.isChecked())
                str +=getString(R.string.painting);
            msg.setText(str);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
